const button = document.querySelector('button');

button.addEventListener('click', findIP)

async function findIP(){
    const {ip} = await fetch('https://api.ipify.org/?format=json').then(response => response.json())
        
    const {continent, country, region, city, district} = await fetch(`http://ip-api.com/json/${ip}`).then(resp => resp.json())

    button.insertAdjacentHTML('afterend', 
    `<div>Континент: ${continent}</div>
    <div>Країна: ${country}</div>
    <div>Регіон: ${region}</div>
    <div>Місто: ${city}</div>
    <div>Район: ${district}</div>`)    
}